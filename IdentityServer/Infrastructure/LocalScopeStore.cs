﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using IdentityServer.DbContexts;
using IdentityServer.Helpers;
using IdentityServer.Models;
using IdentityServer3.Core.Models;
using IdentityServer3.Core.Services;
using Newtonsoft.Json;

namespace IdentityServer.Infrastructure
{
    public class LocalScopeStore : IScopeStore
    {
        private readonly IsContext _db = new IsContext();
        private Scope _destScope;
        private ScopeProfile _sourceScope;

        public async Task<IEnumerable<Scope>> FindScopesAsync(IEnumerable<string> scopeNames)
        {
            var sListTask = _db.ScopeProfiles.Where(a => scopeNames.Contains(a.Name)).ToListAsync();
            await sListTask;

            var spList = sListTask.Result;

            var sList = !spList.Any() ? new List<Scope>() : ConvertToScopeList(spList);

            sList.AddRange(StandardScopes.All);

            return sList;
        }

        public async Task<IEnumerable<Scope>> GetScopesAsync(bool publicOnly = true)
        {
            Task<List<ScopeProfile>> sListTask;

            if (publicOnly)
            {
                sListTask = _db.ScopeProfiles.Where(a => a.Enabled).ToListAsync();
                await sListTask;
            }
            else
            {
                sListTask = _db.ScopeProfiles.Select(a => a).ToListAsync();
                await sListTask;
            }

            var spList = sListTask.Result;

            var sList = !spList.Any() ? new List<Scope>() : ConvertToScopeList(spList);
            sList.AddRange(StandardScopes.All);
            return sList;
            
        }

        private List<Scope> ConvertToScopeList(IEnumerable<ScopeProfile> spList)
        {
            return spList.Select(ConvertToScope).ToList();
        }
        private Scope ConvertToScope(ScopeProfile spItem)
        {
            if (spItem == null) return new Scope();

            _sourceScope = spItem;

            BuildBaseScope();
            BuildStringItems();
            BuildSwitchItems();
            BuildJsonItems();

            var sItem = _destScope;

            _destScope = null;
            _sourceScope = null;

            return sItem;
        }
        private void BuildBaseScope()
        {
            _destScope = new Scope()
            {
                Enabled = _sourceScope.Enabled,
                IncludeAllClaimsForUser = _sourceScope.IncludeAllClaimsForUser,
                Emphasize = _sourceScope.Emphasize,
                ShowInDiscoveryDocument = _sourceScope.ShowInDiscoveryDocument,
                Required = _sourceScope.Required,
                AllowUnrestrictedIntrospection = _sourceScope.AllowUnrestrictedIntrospection
            };

        }
        private void BuildStringItems()
        {
            if (Utils.IsNotNull(_sourceScope.Name)) _destScope.Name = _sourceScope.Name;
            if (Utils.IsNotNull(_sourceScope.DisplayName)) _destScope.DisplayName = _sourceScope.DisplayName;
            if (Utils.IsNotNull(_sourceScope.ClaimsRule)) _destScope.ClaimsRule = _sourceScope.ClaimsRule;
            if (Utils.IsNotNull(_sourceScope.Description)) _destScope.Description = _sourceScope.Description;
        }
        private void BuildSwitchItems()
        {


            if (Utils.IsNotNull(_sourceScope.Type))
            {
                switch (_sourceScope.Type.ToUpper())
                {
                    case "IDENTITY":
                        _destScope.Type = ScopeType.Identity;
                        break;
                    case "RESOURCE":
                        _destScope.Type = ScopeType.Resource;
                        break;
                }
            }


        }
        private void BuildJsonItems()
        {
            if (Utils.IsNotNull(_sourceScope.Claims))
            {
                try
                {
                    _destScope.Claims = JsonConvert.DeserializeObject<List<ScopeClaim>>(_sourceScope.Claims);
                }
                catch
                {
                    _destScope.Claims = null;
                }
            }

            if (Utils.IsNotNull(_sourceScope.ScopeSecrets))
            {
                try
                {
                    _destScope.ScopeSecrets =
                        JsonConvert.DeserializeObject<List<Secret>>(_sourceScope.ScopeSecrets);
                }
                catch
                {
                    _destScope.ScopeSecrets = null;
                }
            }
        }
    }
}