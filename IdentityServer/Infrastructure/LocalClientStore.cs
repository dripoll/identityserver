﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using IdentityServer.DbContexts;
using IdentityServer.Helpers;
using IdentityServer.Models;
using IdentityServer3.Core.Models;
using IdentityServer3.Core.Services;
using Newtonsoft.Json;

namespace IdentityServer.Infrastructure
{
    public class LocalClientStore : IClientStore
    {
        private readonly IsContext _db = new IsContext();
        private ClientProfile _sourceClient;
        private Client _destClient;

        public async Task<Client> FindClientByIdAsync(string clientId)
        {
            var cItemTask = _db.ClientProfiles.FirstOrDefaultAsync(a => a.ClientId == clientId);
            await cItemTask;
            var cItem = cItemTask.Result;
            return cItem == null ? new Client() : ConvertToClient(cItem);
        }

        private Client ConvertToClient(ClientProfile cpItem)
        {

            if (cpItem == null) return new Client();

            _sourceClient = cpItem;

            BuildBaseClient();
            BuildIntItems();
            BuildStringItems();
            BuildSwitchItems();
            BuildJsonItems();
            BuildJsonItems1();

            var cItem = _destClient;

            _destClient = null;
            _sourceClient = null;

            return cItem;
        }
        private void BuildBaseClient()
        {
            _destClient = new Client()
            {
                Enabled = _sourceClient.Enabled,
                LogoutSessionRequired = _sourceClient.LogoutSessionRequired,
                EnableLocalLogin = _sourceClient.EnableLocalLogin,
                AllowAccessToAllCustomGrantTypes = _sourceClient.AllowAccessToAllCustomGrantTypes,
                AllowAccessToAllScopes = _sourceClient.AllowAccessToAllScopes,
                AllowClientCredentialsOnly = _sourceClient.AllowClientCredentialsOnly,
                AlwaysSendClientClaims = _sourceClient.AlwaysSendClientClaims,
                IncludeJwtId = _sourceClient.IncludeJwtId,
                PrefixClientClaims = _sourceClient.PrefixClientClaims,
                RequireConsent = _sourceClient.RequireConsent,
                AllowRememberConsent = _sourceClient.AllowRememberConsent,
                UpdateAccessTokenClaimsOnRefresh = _sourceClient.UpdateAccessTokenClaimsOnRefresh
            };

        }
        private void BuildIntItems()
        {
            if (_sourceClient.IdentityTokenLifetime.HasValue)
                _destClient.IdentityTokenLifetime = (int)_sourceClient.IdentityTokenLifetime;

            if (_sourceClient.AuthorizationCodeLifetime.HasValue)
                _destClient.AuthorizationCodeLifetime = (int)_sourceClient.AuthorizationCodeLifetime;

            if (_sourceClient.AccessTokenLifetime.HasValue)
                _destClient.AccessTokenLifetime = (int)_sourceClient.AccessTokenLifetime;

            if (_sourceClient.AbsoluteRefreshTokenLifetime.HasValue)
                _destClient.AbsoluteRefreshTokenLifetime = (int)_sourceClient.AbsoluteRefreshTokenLifetime;

            if (_sourceClient.SlidingRefreshTokenLifetime.HasValue)
                _destClient.SlidingRefreshTokenLifetime = (int)_sourceClient.SlidingRefreshTokenLifetime;
        }
        private void BuildStringItems()
        {
            if (Utils.IsNotNull(_sourceClient.ClientId)) _destClient.ClientId = _sourceClient.ClientId;
            if (Utils.IsNotNull(_sourceClient.ClientName)) _destClient.ClientName = _sourceClient.ClientName;
            if (Utils.IsNotNull(_sourceClient.ClientUri)) _destClient.ClientUri = _sourceClient.ClientUri;
            if (Utils.IsNotNull(_sourceClient.LogoUri)) _destClient.LogoUri = _sourceClient.LogoUri;
            if (Utils.IsNotNull(_sourceClient.LogoutUri)) _destClient.LogoutUri = _sourceClient.LogoutUri;
        }
        private void BuildSwitchItems()
        {
            if (Utils.IsNotNull(_sourceClient.Flow))
            {
                switch (_sourceClient.Flow.ToUpper())
                {
                    case "AUTHORIZATIONCODE":
                        _destClient.Flow = Flows.AuthorizationCode;
                        break;
                    case "IMPLICIT":
                        _destClient.Flow = Flows.Implicit;
                        break;
                    case "HYBRID":
                        _destClient.Flow = Flows.Hybrid;
                        break;
                    case "RESOURCEOWNER":
                        _destClient.Flow = Flows.ResourceOwner;
                        break;
                    case "CLIENTCREDENTIALS":
                        _destClient.Flow = Flows.ClientCredentials;
                        break;
                    case "CUSTOM":
                        _destClient.Flow = Flows.Custom;
                        break;
                }
            }

            if (Utils.IsNotNull(_sourceClient.AccessTokenType))
            {
                switch (_sourceClient.AccessTokenType.ToUpper())
                {
                    case "JWT":
                        _destClient.AccessTokenType = AccessTokenType.Jwt;
                        break;
                    case "REFERENCE":
                        _destClient.AccessTokenType = AccessTokenType.Reference;
                        break;
                }
            }

            if (Utils.IsNotNull(_sourceClient.RefreshTokenExpiration))
            {
                switch (_sourceClient.RefreshTokenExpiration.ToUpper())
                {
                    case "ABSOLUTE":
                        _destClient.RefreshTokenExpiration = TokenExpiration.Absolute;
                        break;
                    case "SLIDING":
                        _destClient.RefreshTokenExpiration = TokenExpiration.Sliding;
                        break;
                }
            }

            if (Utils.IsNotNull(_sourceClient.RefreshTokenExpiration))
            {
                switch (_sourceClient.RefreshTokenExpiration.ToUpper())
                {
                    case "ABSOLUTE":
                        _destClient.RefreshTokenExpiration = TokenExpiration.Absolute;
                        break;
                    case "SLIDING":
                        _destClient.RefreshTokenExpiration = TokenExpiration.Sliding;
                        break;
                }
            }

            if (Utils.IsNotNull(_sourceClient.RefreshTokenUsage))
            {
                switch (_sourceClient.RefreshTokenUsage.ToUpper())
                {
                    case "ONETIMEONLY":
                        _destClient.RefreshTokenUsage = TokenUsage.OneTimeOnly;
                        break;
                    case "REUSE":
                        _destClient.RefreshTokenUsage = TokenUsage.ReUse;
                        break;
                }
            }
        }
        private void BuildJsonItems()
        {
            if (Utils.IsNotNull(_sourceClient.ClientSecrets))
            {
                try
                {
                    _destClient.ClientSecrets = JsonConvert.DeserializeObject<List<Secret>>(_sourceClient.ClientSecrets);
                }
                catch
                {
                    _destClient.ClientSecrets = null;
                }
            }

            if (Utils.IsNotNull(_sourceClient.Claims))
            {
                try
                {
                    _destClient.Claims =
                        JsonConvert.DeserializeObject<List<Claim>>(_sourceClient.Claims);
                }
                catch
                {
                    _destClient.Claims = null;
                }
            }

            if (Utils.IsNotNull(_sourceClient.AllowedCorsOrigins))
            {
                try
                {
                    _destClient.AllowedCorsOrigins =
                        JsonConvert.DeserializeObject<List<string>>(_sourceClient.AllowedCorsOrigins);
                }
                catch
                {
                    _destClient.AllowedCorsOrigins = null;
                }
            }

            if (Utils.IsNotNull(_sourceClient.AllowedCustomGrantTypes))
            {
                try
                {
                    _destClient.AllowedCustomGrantTypes =
                        JsonConvert.DeserializeObject<List<string>>(_sourceClient.AllowedCustomGrantTypes);
                }
                catch
                {
                    _destClient.AllowedCustomGrantTypes = null;
                }
            }


        }
        private void BuildJsonItems1()
        {
            if (Utils.IsNotNull(_sourceClient.AllowedScopes))
            {
                try
                {
                    _destClient.AllowedScopes = JsonConvert.DeserializeObject<List<string>>(_sourceClient.AllowedScopes);
                }
                catch
                {
                    _destClient.AllowedScopes = null;
                }
            }

            if (Utils.IsNotNull(_sourceClient.PostLogoutRedirectUris))
            {
                try
                {
                    _destClient.PostLogoutRedirectUris =
                        JsonConvert.DeserializeObject<List<string>>(_sourceClient.PostLogoutRedirectUris);
                }
                catch
                {
                    _destClient.PostLogoutRedirectUris = null;
                }
            }

            if (Utils.IsNotNull(_sourceClient.RedirectUris))
            {
                try
                {
                    _destClient.RedirectUris = JsonConvert.DeserializeObject<List<string>>(_sourceClient.RedirectUris);
                }
                catch
                {
                    _destClient.RedirectUris = null;
                }
            }

            if (Utils.IsNotNull(_sourceClient.IdentityProviderRestrictions))
            {
                try
                {
                    _destClient.IdentityProviderRestrictions =
                        JsonConvert.DeserializeObject<List<string>>(_sourceClient.IdentityProviderRestrictions);
                }
                catch
                {
                    _destClient.IdentityProviderRestrictions = null;
                }
            }
        }
        
    }
}