﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IdentityServer.Models
{
    [Table("is_Users")]
    public class UserProfile
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public bool Enabled { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Subject { get; set; }
        public string Claims { get; set; }
        public string ProviderId { get; set; }
        public string Provider { get; set; }
    }

    public class UserLoginModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string ClientId { get; set; }
        public string Scope { get; set; }
    }

    public class UserChangePasswordModel
    {
        public string UserName { get; set; }
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
        public string ConfirmNewPassword { get; set; }
    }

    public class ClientLoginModel
    {
        public string ClientId { get; set; }
        public string Scope { get; set; }
        public string Secret { get; set; }
    }

    public class LocalUserModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public ClaimModel Claims { get; set; }
        public bool IsEnabled { get; set; }
    }

    public class ClaimModel
    {
        public string FirstName { get; set; } //Given Name
        public string MiddleName { get; set; } //Middle Name
        public string LastName { get; set; } //Family Name
        public string FullName { get; set; } //Name
        public string NickName { get; set; } //Nick Name
        public string Address { get; set; } //Address
        public DateTime? BirthDate { get; set; } //Birthdate
        public string Gender { get; set; } //Gender
        public string Email { get; set; } //Email
        public bool EmailVerified { get; set; } //Email Verified
        public string PhoneNumber { get; set; } //Phone Number
        public string PhotoUrl { get; set; } //Picture
        public DateTime? Expiration { get; set; } //Expiration
        public List<string> ExternalProviderUserIds { get; set; } //External Provider User Ids
        public List<string> Roles { get; set; } //Role
        public List<string> Scopes { get; set; } //Scope 
        public List<string> Clients { get; set; } //Client Ids
    }

}