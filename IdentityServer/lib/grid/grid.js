define(['knockout'], function(ko) {
    var Grid = function (configuration) {
      
        var selectedItem = ko.observable(undefined);
        var data = ko.observableArray(configuration.data);
        
        var sortDesc = false;
        var sortKey = "";
        
        var filterText = ko.observable();
        
        this.columns = configuration.columns;

        this.items = ko.computed(function() {
            return data.slice(0).filter(function(item) {
              
              if (!filterText()) return true;
              
              for (var key in item) {
                
                  if(item[key].toLowerCase().indexOf(filterText().toLowerCase()) != -1) {
                      return true;
                  }
              }
            });
            
        }, this);
        
        this.isSelected = function(item) {
          return selectedItem === item;
        };
        
        this.sort = function(column) {
          
          if (sortKey === column.rowText)
            sortDesc = !sortDesc;
          else 
            sortKey = column.rowText;
          
          data.sort(function(a, b){
            if (sortDesc) 
              return a[sortKey] > b[sortKey] ? -1 : 1;
            else 
              return a[sortKey] < b[sortKey] ? -1 : 1;
          });
        };
        
        this.setFilter = function(text) {
            filterText(text);
        };
    };

    Grid.prototype.getColumnsForScaffolding = function(data) {
        if ((typeof data.length !== 'number') || data.length === 0) {
            return [];
        }
        var columns = [];
        for (var propertyName in data[0]) {
            columns.push({ headerText: propertyName, rowText: propertyName });
        }
        return columns;
    };

    return Grid;
});