﻿/** 
 * @module Authentication module. 
 *         All the interactions with users should start from this module
 * @requires system
 * @requires router
 * @requires routeconfig
 * @requires utils
 */

define(["durandal/system", "durandal/app", "plugins/router", "services/routeconfig", "services/settings", "knockout"],
    function (system, app, router, routeconfig, settings) {
        var ko = require('knockout');
        var self = this;

        var userInfo = ko.observable();

        function setUserInfo(userName, roles, perms, isAuthenticated, changePassword) {
            var obj = { userName: userName, roles: roles, perms: perms, isAuthenticated: isAuthenticated, changePassword: changePassword };
            userInfo(obj);
           // logger.logSuccess('Set User Info', obj, null, null);
        };
        function isAuthenticated(cname) {
            var name = cname + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') c = c.substring(1);
                if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
            }
            return "";
        }

         function logout() {
            document.cookie = 'Authenticated = ;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
            router.reset();
            router.deactivate();
            app.setRoot('account/login/index','entrance');
        }

        function setAuth(cname, cvalue, exdays) {
            var d = new Date();
            d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
            var expires = "expires=" + d.toUTCString();
            document.cookie = cname + "=" + cvalue + "; " + expires;
        }

        function initializeAuth() {

            return system.defer(function (dfd) {

                $.ajax(routeconfig.isAuthenticatedUser, {
                    type: "GET",
                    dataType: "json",
                    success: function (jqXhr, error, statusText) {
                        if (jqXhr) {
                            var jObj = JSON.parse(jqXhr);
                          //  logger.logSuccess('Checked if user is authenticated', jObj.IsAuthenticated, null, false);
                            setUserInfo(jObj.UserName, jObj.Roles, jObj.Permissions, jObj.IsAuthenticated, jObj.ChangePassword);
                        }
                        dfd.resolve(true);
                    },
                    error: function (jqXhr, error, statusText) {
                        if (jqXhr) {
                            if (jqXhr.responseText) {
                                var resp = JSON.parse(jqXhr.responseText);
                              //  logger.logError(statusText + " - " + resp.Message, error, null, true);
                            }
                            else {
                               // logger.logError(statusText, error, null, true);
                            }
                        }
                        dfd.resolve(true);
                    }
                });
            }).promise();

        };
        

        function hasRole(role) {

            var cUser = userInfo();

            if (cUser.userName === "admin") return true;

            var uRoles = cUser.roles;
            var found = false;

            $.each(uRoles, function (index, item) {
                if (item === role) found = true;
            });

            if (found) return true;

            return false;

        };

        function hasPermission(perm) {

            var cUser = userInfo();

            if (cUser.userName == "admin") return true;

            var uPerms = cUser.perms;
            var found = false;

            if (!uPerms) return false;

            $.each(uPerms, function (index, item) {
                if (item === perm) found = true;
            });

            if (found) return true;

            return false;

        };

        function checkSession(callback) {

            $.ajax(routeconfig.getSessionState, {
                type: "GET",
                dataType: "json",
                async: false,
                success: callback
            });
        };

        var securitymodel = {
            
            //Variables
            userInfo: userInfo,

            //Functions
            setUserInfo: setUserInfo,
            initializeAuth: initializeAuth,
            hasRole: hasRole,
            hasPermission: hasPermission,
            checkSession: checkSession,
            isAuthenticated: isAuthenticated,
            logout : logout,
            setAuth: setAuth
        };

        return securitymodel;
    });