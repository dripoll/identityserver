﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Web.Mvc;
using IdentityServer.Helpers;
using IdentityServer.Models;
using IdentityServer.DbContexts;
using Newtonsoft.Json;

namespace IdentityServer.Controllers
{
    
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [Authorize]
        public ActionResult Login()
        {
            var cUser = User as ClaimsPrincipal;
            return cUser != null ? View(cUser.Claims) : View();
        }

        [HttpGet]
        public ActionResult Register(string signin)
        {
            return View();
        }

        [HttpPost]
        public ActionResult Register(string signin, LocalUserModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var db = new IsContext();

                    var user = new UserProfile()
                    {
                        Username = model.UserName,
                        Password = Utils.Encrypt(model.Password),
                        Subject = Guid.NewGuid().ToString(),
                    };

                    user.Claims = Utils.ClaimModelToString(model.Claims);                   

                    db.UserProfiles.Add(user);
                    db.SaveChanges();

                    return Redirect("~/");
                }
                catch
                {
                    return View();
                }
            }

            return View();
        }

    }
}