﻿using System.Web.Optimization;

namespace IdentityServer
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/modernizr")
                        .Include("~/Scripts/modernizr-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jquery")
                        .Include("~/Scripts/jquery-{version}.js"));

            bundles.Add(new StyleBundle("~/Content/css")
                        .Include("~/Content/site.css")
                        .Include("~/Content/pagedlist.css")
                        .Include("~/Content/multiselect.css")
                        .Include("~/Content/uploadlist.css"));

            bundles.Add(new StyleBundle("~/Content/themes/base/css")
                        .Include("~/Content/themes/base/jquery.ui.core.css")
                        .Include("~/Content/themes/base/jquery.ui.resizable.css")
                        .Include("~/Content/themes/base/jquery.ui.selectable.css")
                        .Include("~/Content/themes/base/jquery.ui.accordion.css")
                        .Include("~/Content/themes/base/jquery.ui.autocomplete.css")
                        .Include("~/Content/themes/base/jquery.ui.button.css")
                        .Include("~/Content/themes/base/jquery.ui.dialog.css")
                        .Include("~/Content/themes/base/jquery.ui.slider.css")
                        .Include("~/Content/themes/base/jquery.ui.tabs.css")
                        .Include("~/Content/themes/base/jquery.ui.datepicker.css")
                        .Include("~/Content/themes/base/jquery.ui.progressbar.css")
                        .Include("~/Content/themes/base/jquery.ui.theme.css"));

            //------------------------------------------------------------
            //  SPA Bundles
            //------------------------------------------------------------

            bundles.Add(new ScriptBundle("~/bundles/vendor")
                        .Include("~/Scripts/jquery-{version}.js")
                        .Include("~/Scripts/bootstrap.min.js")
                        .Include("~/Scripts/knockout-{version}.js")
                        .Include("~/Scripts/knockout.validation.js")
                        .Include("~/Scripts/kendo/2014.2.716/jquery.min.js")
                        .Include("~/Scripts/kendo/2014.2.716/kendo.all.min.js")
                        .Include("~/Scripts/toastr.js"));

            bundles.Add(new StyleBundle("~/bundles/css")
                        .Include("~/Content/main.css")
                        .Include("~/Content/login.css")
                        .Include("~/Content/modal.css")
                        .Include("~/Content/durandal.css"));

            bundles.Add(new StyleBundle("~/Content/themes/base/css")
                        .Include("~/Content/themes/base/jquery.ui.core.css")
                        .Include("~/Content/themes/base/jquery.ui.button.css")
                        .Include("~/Content/themes/base/jquery.ui.theme.css"));

            bundles.Add(new StyleBundle("~/Content/kendo/2014.2.716/css")
                        .Include("~/Content/kendo/2014.2.716/kendo.common.min.css")
                        .Include("~/Content/kendo/2014.2.716/kendo.silver.min.css"));
        }
    }
}
