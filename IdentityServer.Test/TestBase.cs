﻿using System;
using System.Text;
using System.Data.SqlClient;
using System.Threading.Tasks;
using IdentityServer.Models;
using NUnit.Framework;

namespace IdentityServer.Test
{
    [TestFixture]
    public class TestBase
    {

        public string UserName = "test.user@test.com";
        public string UserPwd = "Test123!";
        public string UserClient = "test";
        public string TokenClient = "test_service";
        public string TokenSecret = "secret";
        public string TokenScope = "testApi";
        public const string ServerUrl = "https://localhost:44301";
        public const string DbConnect = "Data Source=MNED2K12-WEB1;Initial Catalog=IdentityServer-Dev;User Id=isuser;Password=IdentityServer1234!!";       
        public TestClient Client { get; set; }
        private SqlConnection _conn;

        [TestFixtureSetUp]
        public void TestFixtureSetup()
        {
            _conn = new SqlConnection(DbConnect);

            ResetDatabases();
            BuildDatabases();

            var setupCfg = new ClientSetupModel()
            {
                UserName = UserName,
                UserPwd = UserPwd,
                UserClient = UserClient,
                TokenClient = TokenClient,
                TokenSecret = TokenSecret,
                TokenScope = TokenScope,
                ServerUrl = ServerUrl
            };

            Client = new TestClient(setupCfg);

        }

        [Test]
        public void ClientIsNotNull()
        {
            if (Client != null) Console.WriteLine(@"Client: OK");
            if (Client == null) Console.WriteLine(@"Client: NULL");
            Assert.IsNotNull(Client);
        }

        public async Task<bool> LoginLocalUser()
        {
            var values = new UserLoginModel()
            {
                UserName = UserName,
                Password = UserPwd,
                ClientId = UserClient
            };

            var response = await Client.LoginLocalUser(values);
            Assert.IsTrue(response);
            return true;
        }

        public async Task<bool> LogoutLocalUser()
        {
            var response = await Client.LogoutLocalUser();
            Assert.IsTrue(response);
            return true;
        }

        public async Task<bool> LoginClientUser()
        {
            var values = new ClientLoginModel()
            {
                ClientId = TokenClient,
                Secret = TokenSecret,
                Scope = TokenScope
            };

            var response = await Client.LoginClientUser(values);
            Assert.IsTrue(response);
            return true;
        }

        public void ResetDatabases()
        {
            try
            {
                _conn.Open();

                var sqlCmd = new SqlCommand(
                    "ALTER TABLE [dbo].[is_Users] DROP CONSTRAINT [DF__is_Users__Enable__47A6A41B]", _conn);
                sqlCmd.ExecuteNonQuery();

                sqlCmd = new SqlCommand(
                    "ALTER TABLE [dbo].[is_Scopes] DROP CONSTRAINT [DF__is_Scopes__Allow__30C33EC3]", _conn);
                sqlCmd.ExecuteNonQuery();

                sqlCmd = new SqlCommand(
                    "ALTER TABLE [dbo].[is_Scopes] DROP CONSTRAINT [DF__is_Scopes__Requi__2FCF1A8A]", _conn);
                sqlCmd.ExecuteNonQuery();

                sqlCmd = new SqlCommand(
                    "ALTER TABLE [dbo].[is_Scopes] DROP CONSTRAINT [DF__is_Scopes__ShowI__2EDAF651]", _conn);
                sqlCmd.ExecuteNonQuery();

                sqlCmd = new SqlCommand(
                    "ALTER TABLE [dbo].[is_Scopes] DROP CONSTRAINT [DF__is_Scopes__Empha__2DE6D218]", _conn);
                sqlCmd.ExecuteNonQuery();

                sqlCmd = new SqlCommand(
                    "ALTER TABLE [dbo].[is_Scopes] DROP CONSTRAINT [DF__is_Scopes__Inclu__2CF2ADDF]", _conn);
                sqlCmd.ExecuteNonQuery();

                sqlCmd = new SqlCommand(
                    "ALTER TABLE [dbo].[is_Scopes] DROP CONSTRAINT [DF__is_Scopes__Enabl__2BFE89A6]", _conn);
                sqlCmd.ExecuteNonQuery();

                sqlCmd = new SqlCommand(
                    "ALTER TABLE [dbo].[is_Clients] DROP CONSTRAINT [DF__is_Client__Updat__1EA48E88]", _conn);
                sqlCmd.ExecuteNonQuery();

                sqlCmd = new SqlCommand(
                    "ALTER TABLE [dbo].[is_Clients] DROP CONSTRAINT [DF__is_Client__Allow__1DB06A4F]", _conn);
                sqlCmd.ExecuteNonQuery();

                sqlCmd = new SqlCommand(
                    "ALTER TABLE [dbo].[is_Clients] DROP CONSTRAINT [DF__is_Client__Requi__1CBC4616]", _conn);
                sqlCmd.ExecuteNonQuery();

                sqlCmd = new SqlCommand(
                    "ALTER TABLE [dbo].[is_Clients] DROP CONSTRAINT [DF__is_Client__Prefi__1BC821DD]", _conn);
                sqlCmd.ExecuteNonQuery();

                sqlCmd = new SqlCommand(
                    "ALTER TABLE [dbo].[is_Clients] DROP CONSTRAINT [DF__is_Client__Inclu__1AD3FDA4]", _conn);
                sqlCmd.ExecuteNonQuery();

                sqlCmd = new SqlCommand(
                    "ALTER TABLE [dbo].[is_Clients] DROP CONSTRAINT [DF__is_Client__Alway__19DFD96B]", _conn);
                sqlCmd.ExecuteNonQuery();

                sqlCmd = new SqlCommand(
                    "ALTER TABLE [dbo].[is_Clients] DROP CONSTRAINT [DF__is_Client__Allow__18EBB532]", _conn);
                sqlCmd.ExecuteNonQuery();

                sqlCmd = new SqlCommand(
                    "ALTER TABLE [dbo].[is_Clients] DROP CONSTRAINT [DF__is_Client__Allow__17F790F9]", _conn);
                sqlCmd.ExecuteNonQuery();

                sqlCmd = new SqlCommand(
                    "ALTER TABLE [dbo].[is_Clients] DROP CONSTRAINT [DF__is_Client__Allow__17036CC0]", _conn);
                sqlCmd.ExecuteNonQuery();

                sqlCmd = new SqlCommand(
                    "ALTER TABLE [dbo].[is_Clients] DROP CONSTRAINT [DF__is_Client__Enabl__160F4887]", _conn);
                sqlCmd.ExecuteNonQuery();

                sqlCmd = new SqlCommand(
                    "ALTER TABLE [dbo].[is_Clients] DROP CONSTRAINT [DF__is_Client__Logou__151B244E]", _conn);
                sqlCmd.ExecuteNonQuery();

                sqlCmd = new SqlCommand(
                    "ALTER TABLE [dbo].[is_Clients] DROP CONSTRAINT [DF__is_Client__Enabl__14270015]", _conn);
                sqlCmd.ExecuteNonQuery();

                sqlCmd = new SqlCommand(
                    "ALTER TABLE [dbo].[is_Users] DROP CONSTRAINT [UQ__is_Users__A2B1D904623BC68A]", _conn);
                sqlCmd.ExecuteNonQuery();

                sqlCmd = new SqlCommand(
                    "ALTER TABLE [dbo].[is_Users] DROP CONSTRAINT [UQ__is_Users__536C85E417B98590]", _conn);
                sqlCmd.ExecuteNonQuery();

                sqlCmd = new SqlCommand(
                    "ALTER TABLE [dbo].[is_Scopes] DROP CONSTRAINT [UQ__is_Scope__737584F6CB8B8DC6]", _conn);
                sqlCmd.ExecuteNonQuery();

                sqlCmd = new SqlCommand(
                    "ALTER TABLE [dbo].[is_Clients] DROP CONSTRAINT [UQ__is_Clien__E67E1A254E55E07B]", _conn);
                sqlCmd.ExecuteNonQuery();

                sqlCmd = new SqlCommand(
                    "DROP TABLE [dbo].[is_Users]", _conn);
                sqlCmd.ExecuteNonQuery();

                sqlCmd = new SqlCommand(
                    "DROP TABLE [dbo].[is_Scopes]", _conn);
                sqlCmd.ExecuteNonQuery();

                sqlCmd = new SqlCommand(
                    "DROP TABLE [dbo].[is_Clients]", _conn);
                sqlCmd.ExecuteNonQuery();
                
            }
            catch
            {
                Console.WriteLine(@"SQL Failed");
            }
            finally
            {
                _conn.Close();
            }            

        }

        public void BuildDatabases()
        {
            try
            {
                _conn.Open();

                var sqlCmd = new SqlCommand(
                    "SET ANSI_NULLS ON", _conn);
                sqlCmd.ExecuteNonQuery();

                sqlCmd = new SqlCommand(
                    "SET QUOTED_IDENTIFIER ON", _conn);
                sqlCmd.ExecuteNonQuery();

                var sb = new StringBuilder();

                sb.Append("CREATE TABLE [dbo].[is_Clients]( ")
                    .Append("[Id] [int] IDENTITY(1,1) NOT NULL, ")
                    .Append("[Enabled] [bit] NOT NULL, ")
                    .Append("[ClientId] [nvarchar](50) NOT NULL, ")
                    .Append("[ClientName] [nvarchar](50) NOT NULL, ")
                    .Append("[ClientSecrets] [nvarchar](max) NULL, ")
                    .Append("[ClientUri] [nvarchar](max) NULL, ")
                    .Append("[LogoUri] [nvarchar](max) NULL, ")
                    .Append("[LogoutSessionRequired] [bit] NOT NULL, ")
                    .Append("[LogoutUri] [nvarchar](max) NULL, ")
                    .Append("[EnableLocalLogin] [bit] NOT NULL, ")
                    .Append("[Flow] [nvarchar](25) NULL, ")
                    .Append("[Claims] [nvarchar](max) NULL, ")
                    .Append("[AllowAccessToAllCustomGrantTypes] [bit] NOT NULL, ")
                    .Append("[AllowAccessToAllScopes] [bit] NOT NULL, ")
                    .Append("[AllowClientCredentialsOnly] [bit] NOT NULL, ")
                    .Append("[AlwaysSendClientClaims] [bit] NOT NULL, ")
                    .Append("[AllowedCorsOrigins] [nvarchar](max) NULL, ")
                    .Append("[AllowedCustomGrantTypes] [nvarchar](max) NULL, ")
                    .Append("[AllowedScopes] [nvarchar](max) NULL, ")
                    .Append("[IncludeJwtId] [bit] NOT NULL, ")
                    .Append("[PostLogoutRedirectUris] [nvarchar](max) NULL, ")
                    .Append("[PrefixClientClaims] [bit] NOT NULL, ")
                    .Append("[RedirectUris] [nvarchar](max) NULL, ")
                    .Append("[RequireConsent] [bit] NOT NULL, ")
                    .Append("[AllowRememberConsent] [bit] NOT NULL, ")
                    .Append("[IdentityProviderRestrictions] [nvarchar](max) NULL, ")
                    .Append("[IdentityTokenLifetime] [int] NULL, ")
                    .Append("[AuthorizationCodeLifetime] [int] NULL, ")
                    .Append("[AccessTokenLifetime] [int] NULL, ")
                    .Append("[AccessTokenType] [nvarchar](10) NULL, ")
                    .Append("[AbsoluteRefreshTokenLifetime] [int] NULL, ")
                    .Append("[SlidingRefreshTokenLifetime] [int] NULL, ")
                    .Append("[RefreshTokenExpiration] [nvarchar](10) NULL, ")
                    .Append("[RefreshTokenUsage] [nvarchar](10) NULL, ")
                    .Append("[UpdateAccessTokenClaimsOnRefresh] [bit] NOT NULL, ")
                    .Append("CONSTRAINT [PK_is_Clients] PRIMARY KEY CLUSTERED ")
                    .Append("( ")
                    .Append("[Id] ASC ")
                    .Append(")WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY] ")
                    .Append(") ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]");

                sqlCmd = new SqlCommand(sb.ToString(), _conn);
                sqlCmd.ExecuteNonQuery();

                sqlCmd = new SqlCommand(
                    "SET ANSI_NULLS ON", _conn);
                sqlCmd.ExecuteNonQuery();

                sqlCmd = new SqlCommand(
                    "SET QUOTED_IDENTIFIER ON", _conn);
                sqlCmd.ExecuteNonQuery();

                sb = new StringBuilder();

                sb.Append("CREATE TABLE [dbo].[is_Scopes]( ")
                    .Append("[Id] [int] IDENTITY(1,1) NOT NULL, ")
                    .Append("[Enabled] [bit] NOT NULL, ")
                    .Append("[Name] [nvarchar](50) NOT NULL, ")
                    .Append("[DisplayName] [nvarchar](50) NULL, ")
                    .Append("[Type] [nvarchar](10) NULL, ")
                    .Append("[Claims] [nvarchar](max) NULL, ")
                    .Append("[ClaimsRule] [nvarchar](max) NULL, ")
                    .Append("[ScopeSecrets] [nvarchar](max) NULL, ")
                    .Append("[Description] [nvarchar](max) NULL, ")
                    .Append("[IncludeAllClaimsForUser] [bit] NOT NULL, ")
                    .Append("[Emphasize] [bit] NOT NULL, ")
                    .Append("[ShowInDiscoveryDocument] [bit] NOT NULL, ")
                    .Append("[Required] [bit] NOT NULL, ")
                    .Append("[AllowUnrestrictedIntrospection] [bit] NOT NULL, ")
                    .Append("CONSTRAINT [PK_is_Scopes] PRIMARY KEY CLUSTERED ")
                    .Append("( ")
                    .Append("[Id] ASC ")
                    .Append(
                        ")WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY] ")
                    .Append(") ON [PRIMARY] TEXTIMAGE_ON [PRIMARY] ");

                sqlCmd = new SqlCommand(sb.ToString(), _conn);
                sqlCmd.ExecuteNonQuery();

                sqlCmd = new SqlCommand(
                    "SET ANSI_NULLS ON", _conn);
                sqlCmd.ExecuteNonQuery();

                sqlCmd = new SqlCommand(
                    "SET QUOTED_IDENTIFIER ON", _conn);
                sqlCmd.ExecuteNonQuery();

                sb = new StringBuilder();

                sb.Append("CREATE TABLE [dbo].[is_Users]( ")
                    .Append("[Id] [int] IDENTITY(1,1) NOT NULL, ")
                    .Append("[Enabled] [bit] NOT NULL, ")
                    .Append("[Username] [nvarchar](50) NOT NULL, ")
                    .Append("[Password] [nvarchar](200) NOT NULL, ")
                    .Append("[Subject] [nvarchar](50) NOT NULL, ")
                    .Append("[Claims] [nvarchar](max) NULL, ")
                    .Append("[ProviderId] [nvarchar](50) NULL, ")
                    .Append("[Provider] [nvarchar](50) NULL, ")
                    .Append("CONSTRAINT [PK_is_Users] PRIMARY KEY CLUSTERED ")
                    .Append("( ")
                    .Append("[Id] ASC ")
                    .Append(
                        ")WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY] ")
                    .Append(") ON [PRIMARY] TEXTIMAGE_ON [PRIMARY] ");

                sqlCmd = new SqlCommand(sb.ToString(), _conn);
                sqlCmd.ExecuteNonQuery();

                sqlCmd = new SqlCommand(
                    "SET IDENTITY_INSERT [dbo].[is_Clients] ON ", _conn);
                sqlCmd.ExecuteNonQuery();

                sqlCmd = new SqlCommand(
                    "INSERT [dbo].[is_Clients] ([Id], [Enabled], [ClientId], [ClientName], [ClientSecrets], [ClientUri], [LogoUri], [LogoutSessionRequired], [LogoutUri], [EnableLocalLogin], [Flow], [Claims], [AllowAccessToAllCustomGrantTypes], [AllowAccessToAllScopes], [AllowClientCredentialsOnly], [AlwaysSendClientClaims], [AllowedCorsOrigins], [AllowedCustomGrantTypes], [AllowedScopes], [IncludeJwtId], [PostLogoutRedirectUris], [PrefixClientClaims], [RedirectUris], [RequireConsent], [AllowRememberConsent], [IdentityProviderRestrictions], [IdentityTokenLifetime], [AuthorizationCodeLifetime], [AccessTokenLifetime], [AccessTokenType], [AbsoluteRefreshTokenLifetime], [SlidingRefreshTokenLifetime], [RefreshTokenExpiration], [RefreshTokenUsage], [UpdateAccessTokenClaimsOnRefresh]) VALUES (1, 1, N'test', N'Test Client', NULL, NULL, NULL, 1, NULL, 1, N'Implicit', NULL, 0, 1, 0, 0, NULL, NULL, NULL, 0, NULL, 0, N'[\"https://localhost:44301/\"]', 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0)",
                    _conn);
                sqlCmd.ExecuteNonQuery();

                sqlCmd = new SqlCommand(
                    "INSERT [dbo].[is_Clients] ([Id], [Enabled], [ClientId], [ClientName], [ClientSecrets], [ClientUri], [LogoUri], [LogoutSessionRequired], [LogoutUri], [EnableLocalLogin], [Flow], [Claims], [AllowAccessToAllCustomGrantTypes], [AllowAccessToAllScopes], [AllowClientCredentialsOnly], [AlwaysSendClientClaims], [AllowedCorsOrigins], [AllowedCustomGrantTypes], [AllowedScopes], [IncludeJwtId], [PostLogoutRedirectUris], [PrefixClientClaims], [RedirectUris], [RequireConsent], [AllowRememberConsent], [IdentityProviderRestrictions], [IdentityTokenLifetime], [AuthorizationCodeLifetime], [AccessTokenLifetime], [AccessTokenType], [AbsoluteRefreshTokenLifetime], [SlidingRefreshTokenLifetime], [RefreshTokenExpiration], [RefreshTokenUsage], [UpdateAccessTokenClaimsOnRefresh]) VALUES (2, 1, N'test_service', N'Test Client (service communication)', N'[{\"Value\": \"K7gNU3sdo+OL0wNhqoVWhr3g6s1xYv72ol/pe/Unols=\"}]', NULL, NULL, 1, NULL, 1, N'ClientCredentials', NULL, 0, 0, 0, 0, NULL, NULL, N'[\"testApi\"]', 0, NULL, 0, NULL, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0)",
                    _conn);
                sqlCmd.ExecuteNonQuery();

                sqlCmd = new SqlCommand(
                    "INSERT [dbo].[is_Clients] ([Id], [Enabled], [ClientId], [ClientName], [ClientSecrets], [ClientUri], [LogoUri], [LogoutSessionRequired], [LogoutUri], [EnableLocalLogin], [Flow], [Claims], [AllowAccessToAllCustomGrantTypes], [AllowAccessToAllScopes], [AllowClientCredentialsOnly], [AlwaysSendClientClaims], [AllowedCorsOrigins], [AllowedCustomGrantTypes], [AllowedScopes], [IncludeJwtId], [PostLogoutRedirectUris], [PrefixClientClaims], [RedirectUris], [RequireConsent], [AllowRememberConsent], [IdentityProviderRestrictions], [IdentityTokenLifetime], [AuthorizationCodeLifetime], [AccessTokenLifetime], [AccessTokenType], [AbsoluteRefreshTokenLifetime], [SlidingRefreshTokenLifetime], [RefreshTokenExpiration], [RefreshTokenUsage], [UpdateAccessTokenClaimsOnRefresh]) VALUES (3, 1, N'maxrte', N'MaxRte Client', NULL, NULL, NULL, 1, NULL, 1, N'Implicit', NULL, 0, 1, 0, 0, NULL, NULL, NULL, 0, NULL, 0, N'[\"https://localhost:44301/\"]', 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0)",
                    _conn);
                sqlCmd.ExecuteNonQuery();

                sqlCmd = new SqlCommand(
                    "INSERT [dbo].[is_Clients] ([Id], [Enabled], [ClientId], [ClientName], [ClientSecrets], [ClientUri], [LogoUri], [LogoutSessionRequired], [LogoutUri], [EnableLocalLogin], [Flow], [Claims], [AllowAccessToAllCustomGrantTypes], [AllowAccessToAllScopes], [AllowClientCredentialsOnly], [AlwaysSendClientClaims], [AllowedCorsOrigins], [AllowedCustomGrantTypes], [AllowedScopes], [IncludeJwtId], [PostLogoutRedirectUris], [PrefixClientClaims], [RedirectUris], [RequireConsent], [AllowRememberConsent], [IdentityProviderRestrictions], [IdentityTokenLifetime], [AuthorizationCodeLifetime], [AccessTokenLifetime], [AccessTokenType], [AbsoluteRefreshTokenLifetime], [SlidingRefreshTokenLifetime], [RefreshTokenExpiration], [RefreshTokenUsage], [UpdateAccessTokenClaimsOnRefresh]) VALUES (6, 1, N'maxrte_service', N'MaxRte Client (service communication)', N'[{\"Value\": \"K7gNU3sdo+OL0wNhqoVWhr3g6s1xYv72ol/pe/Unols=\"}]', NULL, NULL, 1, NULL, 1, N'ClientCredentials', NULL, 0, 0, 0, 0, NULL, NULL, N'[\"maxrteApi\"]', 0, NULL, 0, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0)",
                    _conn);
                sqlCmd.ExecuteNonQuery();

                sqlCmd = new SqlCommand(
                    "INSERT [dbo].[is_Clients] ([Id], [Enabled], [ClientId], [ClientName], [ClientSecrets], [ClientUri], [LogoUri], [LogoutSessionRequired], [LogoutUri], [EnableLocalLogin], [Flow], [Claims], [AllowAccessToAllCustomGrantTypes], [AllowAccessToAllScopes], [AllowClientCredentialsOnly], [AlwaysSendClientClaims], [AllowedCorsOrigins], [AllowedCustomGrantTypes], [AllowedScopes], [IncludeJwtId], [PostLogoutRedirectUris], [PrefixClientClaims], [RedirectUris], [RequireConsent], [AllowRememberConsent], [IdentityProviderRestrictions], [IdentityTokenLifetime], [AuthorizationCodeLifetime], [AccessTokenLifetime], [AccessTokenType], [AbsoluteRefreshTokenLifetime], [SlidingRefreshTokenLifetime], [RefreshTokenExpiration], [RefreshTokenUsage], [UpdateAccessTokenClaimsOnRefresh]) VALUES (7, 1, N'dash', N'Dash Client', NULL, NULL, NULL, 1, NULL, 1, N'Implicit', NULL, 0, 1, 0, 0, NULL, NULL, NULL, 0, NULL, 0, N'[\"https://localhost:44301/\"]', 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0)",
                    _conn);
                sqlCmd.ExecuteNonQuery();

                sqlCmd = new SqlCommand(
                    "SET IDENTITY_INSERT [dbo].[is_Clients] OFF", _conn);
                sqlCmd.ExecuteNonQuery();

                sqlCmd = new SqlCommand(
                    "SET IDENTITY_INSERT [dbo].[is_Scopes] ON ", _conn);
                sqlCmd.ExecuteNonQuery();

                sqlCmd = new SqlCommand(
                    "INSERT [dbo].[is_Scopes] ([Id], [Enabled], [Name], [DisplayName], [Type], [Claims], [ClaimsRule], [ScopeSecrets], [Description], [IncludeAllClaimsForUser], [Emphasize], [ShowInDiscoveryDocument], [Required], [AllowUnrestrictedIntrospection]) VALUES (1, 1, N'roles', NULL, N'Identity', N'[{\"Name\": \"role\"}]', NULL, NULL, NULL, 0, 0, 1, 0, 0)",
                    _conn);
                sqlCmd.ExecuteNonQuery();

                sqlCmd = new SqlCommand(
                    "INSERT [dbo].[is_Scopes] ([Id], [Enabled], [Name], [DisplayName], [Type], [Claims], [ClaimsRule], [ScopeSecrets], [Description], [IncludeAllClaimsForUser], [Emphasize], [ShowInDiscoveryDocument], [Required], [AllowUnrestrictedIntrospection]) VALUES (2, 1, N'testApi', N'Test API', N'Resource', NULL, NULL, NULL, N'Access to Test API', 0, 0, 1, 0, 0)",
                    _conn);
                sqlCmd.ExecuteNonQuery();

                sqlCmd = new SqlCommand(
                    "INSERT [dbo].[is_Scopes] ([Id], [Enabled], [Name], [DisplayName], [Type], [Claims], [ClaimsRule], [ScopeSecrets], [Description], [IncludeAllClaimsForUser], [Emphasize], [ShowInDiscoveryDocument], [Required], [AllowUnrestrictedIntrospection]) VALUES (3, 1, N'scopes', NULL, N'Identity', N'[{\"Name\": \"scope\"}]', NULL, NULL, NULL, 0, 0, 1, 0, 0)",
                    _conn);
                sqlCmd.ExecuteNonQuery();

                sqlCmd = new SqlCommand(
                    "INSERT [dbo].[is_Scopes] ([Id], [Enabled], [Name], [DisplayName], [Type], [Claims], [ClaimsRule], [ScopeSecrets], [Description], [IncludeAllClaimsForUser], [Emphasize], [ShowInDiscoveryDocument], [Required], [AllowUnrestrictedIntrospection]) VALUES (4, 1, N'clientids', NULL, N'Identity', N'[{\"Name\": \"client_id\"}]', NULL, NULL, NULL, 0, 0, 1, 0, 0)",
                    _conn);
                sqlCmd.ExecuteNonQuery();

                sqlCmd = new SqlCommand(
                    "INSERT [dbo].[is_Scopes] ([Id], [Enabled], [Name], [DisplayName], [Type], [Claims], [ClaimsRule], [ScopeSecrets], [Description], [IncludeAllClaimsForUser], [Emphasize], [ShowInDiscoveryDocument], [Required], [AllowUnrestrictedIntrospection]) VALUES (5, 1, N'maxrteApi', N'MaxRte API', N'Resource', NULL, NULL, NULL, N'Access to MaxRte API', 0, 0, 1, 0, 0)",
                    _conn);
                sqlCmd.ExecuteNonQuery();

                sqlCmd = new SqlCommand(
                    "SET IDENTITY_INSERT [dbo].[is_Scopes] OFF", _conn);
                sqlCmd.ExecuteNonQuery();

                sqlCmd = new SqlCommand(
                    "SET IDENTITY_INSERT [dbo].[is_Users] ON ", _conn);
                sqlCmd.ExecuteNonQuery();

                sqlCmd = new SqlCommand(
                    "INSERT [dbo].[is_Users] ([Id], [Enabled], [Username], [Password], [Subject], [Claims], [ProviderId], [Provider]) VALUES (1, 1, N'test.user@test.com', N'HOckco2T6IC1BlytwbiodYXYZS8v/gPrMgSPYO/g5gM=', N'e9fd8000-77eb-4b72-9303-579f40faa2f4', N'[{\"Key\":\"GivenName\",\"Value\":\"Test\"},{\"Key\":\"FamilyName\",\"Value\":\"User\"},{\"Key\":\"Email\",\"Value\":\"test.user@test.com\"},{\"Key\":\"EmailVerified\",\"Value\":\"False\"},{\"Key\":\"Scope\",\"Value\":\"testApi\"},{\"Key\":\"ClientId\",\"Value\":\"test\"},{\"Key\":\"ClientId\",\"Value\":\"test_service\"}]', NULL, NULL)",
                    _conn);
                sqlCmd.ExecuteNonQuery();

                sqlCmd = new SqlCommand(
                    "SET IDENTITY_INSERT [dbo].[is_Users] OFF", _conn);
                sqlCmd.ExecuteNonQuery();

                sqlCmd = new SqlCommand(
                    "SET ANSI_PADDING ON", _conn);
                sqlCmd.ExecuteNonQuery();

                sqlCmd = new SqlCommand(
                    "ALTER TABLE [dbo].[is_Clients] ADD UNIQUE NONCLUSTERED ([ClientId] ASC)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY];",
                    _conn);
                sqlCmd.ExecuteNonQuery();

                sqlCmd = new SqlCommand(
                    "SET ANSI_PADDING ON", _conn);
                sqlCmd.ExecuteNonQuery();

                sqlCmd = new SqlCommand(
                    "ALTER TABLE [dbo].[is_Scopes] ADD UNIQUE NONCLUSTERED ([Name] ASC)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY];",
                    _conn);
                sqlCmd.ExecuteNonQuery();

                sqlCmd = new SqlCommand(
                    "SET ANSI_PADDING ON", _conn);
                sqlCmd.ExecuteNonQuery();

                sqlCmd = new SqlCommand(
                    "ALTER TABLE [dbo].[is_Users] ADD UNIQUE NONCLUSTERED ([Username] ASC)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY];",
                    _conn);
                sqlCmd.ExecuteNonQuery();

                sqlCmd = new SqlCommand(
                    "SET ANSI_PADDING ON", _conn);
                sqlCmd.ExecuteNonQuery();

                sqlCmd = new SqlCommand(
                    "ALTER TABLE [dbo].[is_Users] ADD UNIQUE NONCLUSTERED ([Subject] ASC)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY];",
                    _conn);
                sqlCmd.ExecuteNonQuery();

                sqlCmd = new SqlCommand(
                    "ALTER TABLE [dbo].[is_Clients] ADD  DEFAULT ((1)) FOR [Enabled]", _conn);
                sqlCmd.ExecuteNonQuery();

                sqlCmd = new SqlCommand(
                    "ALTER TABLE [dbo].[is_Clients] ADD  DEFAULT ((1)) FOR [LogoutSessionRequired]", _conn);
                sqlCmd.ExecuteNonQuery();

                sqlCmd = new SqlCommand(
                    "ALTER TABLE [dbo].[is_Clients] ADD  DEFAULT ((1)) FOR [EnableLocalLogin]", _conn);
                sqlCmd.ExecuteNonQuery();

                sqlCmd = new SqlCommand(
                    "ALTER TABLE [dbo].[is_Clients] ADD  DEFAULT ((0)) FOR [AllowAccessToAllCustomGrantTypes]", _conn);
                sqlCmd.ExecuteNonQuery();

                sqlCmd = new SqlCommand(
                    "ALTER TABLE [dbo].[is_Clients] ADD  DEFAULT ((0)) FOR [AllowAccessToAllScopes]", _conn);
                sqlCmd.ExecuteNonQuery();

                sqlCmd = new SqlCommand(
                    "ALTER TABLE [dbo].[is_Clients] ADD  DEFAULT ((0)) FOR [AllowClientCredentialsOnly]", _conn);
                sqlCmd.ExecuteNonQuery();

                sqlCmd = new SqlCommand(
                    "ALTER TABLE [dbo].[is_Clients] ADD  DEFAULT ((0)) FOR [AlwaysSendClientClaims]", _conn);
                sqlCmd.ExecuteNonQuery();

                sqlCmd = new SqlCommand(
                    "ALTER TABLE [dbo].[is_Clients] ADD  DEFAULT ((0)) FOR [IncludeJwtId]", _conn);
                sqlCmd.ExecuteNonQuery();

                sqlCmd = new SqlCommand(
                    "ALTER TABLE [dbo].[is_Clients] ADD  DEFAULT ((0)) FOR [PrefixClientClaims]", _conn);
                sqlCmd.ExecuteNonQuery();

                sqlCmd = new SqlCommand(
                    "ALTER TABLE [dbo].[is_Clients] ADD  DEFAULT ((1)) FOR [RequireConsent]", _conn);
                sqlCmd.ExecuteNonQuery();

                sqlCmd = new SqlCommand(
                    "ALTER TABLE [dbo].[is_Clients] ADD  DEFAULT ((1)) FOR [AllowRememberConsent]", _conn);
                sqlCmd.ExecuteNonQuery();

                sqlCmd = new SqlCommand(
                    "ALTER TABLE [dbo].[is_Clients] ADD  DEFAULT ((0)) FOR [UpdateAccessTokenClaimsOnRefresh]", _conn);
                sqlCmd.ExecuteNonQuery();

                sqlCmd = new SqlCommand(
                    "ALTER TABLE [dbo].[is_Scopes] ADD  DEFAULT ((1)) FOR [Enabled]", _conn);
                sqlCmd.ExecuteNonQuery();

                sqlCmd = new SqlCommand(
                    "ALTER TABLE [dbo].[is_Scopes] ADD  DEFAULT ((0)) FOR [IncludeAllClaimsForUser]", _conn);
                sqlCmd.ExecuteNonQuery();

                sqlCmd = new SqlCommand(
                    "ALTER TABLE [dbo].[is_Scopes] ADD  DEFAULT ((0)) FOR [Emphasize]", _conn);
                sqlCmd.ExecuteNonQuery();

                sqlCmd = new SqlCommand(
                    "ALTER TABLE [dbo].[is_Scopes] ADD  DEFAULT ((1)) FOR [ShowInDiscoveryDocument]", _conn);
                sqlCmd.ExecuteNonQuery();

                sqlCmd = new SqlCommand(
                    "ALTER TABLE [dbo].[is_Scopes] ADD  DEFAULT ((0)) FOR [Required]", _conn);
                sqlCmd.ExecuteNonQuery();

                sqlCmd = new SqlCommand(
                    "ALTER TABLE [dbo].[is_Scopes] ADD  DEFAULT ((0)) FOR [AllowUnrestrictedIntrospection]", _conn);
                sqlCmd.ExecuteNonQuery();

                sqlCmd = new SqlCommand(
                    "ALTER TABLE [dbo].[is_Users] ADD  DEFAULT ((1)) FOR [Enabled]", _conn);
                sqlCmd.ExecuteNonQuery();

                
            }
            catch
            {
                Console.WriteLine(@"SQL Failed");
            }
            finally
            {
                _conn.Close();
            }

        }

    }

    public class ClientSetupModel
    {
        public string UserName { get; set; }
        public string UserPwd { get; set; }
        public string UserClient { get; set; }
        public string ServerUrl { get; set; }
        public string TokenClient { get; set; }
        public string TokenSecret { get; set; }
        public string TokenScope { get; set; }
        public string ClientToken { get; set; }
    }

}
