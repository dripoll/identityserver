﻿using System;
using System.Collections.Generic;
using System.Linq;
using IdentityServer.Models;
using Newtonsoft.Json;
using NUnit.Framework;

namespace IdentityServer.Test.Controllers
{
    [TestFixture]
    public class ClientController : TestBase
    {
        [Test]
        public async void Get_Client_Details()
        {
            var response = await Client.GetClientDetails("test");
            var jObj = JsonConvert.DeserializeObject(response);
            var uObj = JsonConvert.DeserializeObject<ClientModel>(jObj.ToString());
            Assert.IsNotNull(uObj);
            Assert.AreEqual("test", uObj.ClientId);

            Console.WriteLine(@"Client Id: " + uObj.ClientId);
        }

        [Test]
        public async void Get_Client_List()
        {
            var response = await Client.GetClientList();
            var jObj = JsonConvert.DeserializeObject(response);
            var uObj = JsonConvert.DeserializeObject<List<ClientModel>>(jObj.ToString());
            Assert.IsNotNull(uObj);

            Console.WriteLine(@"Client Count: " + uObj.Count());

            if (!uObj.Any()) return;

            var uItem = uObj.FirstOrDefault(a => a.ClientId == "test");
            Assert.IsNotNull(uItem);

            Console.WriteLine(@"First Client Id: " + uObj[0].ClientId);
        }

        [Test]
        public async void Add_And_Delete_New_Client()
        {
            //New client model
            var nClient = new ClientModel()
            {
                ClientId = "test_client",
                ClientName = "Test Client",
                Flow = "Implicit",
                RedirectUris = new List<string> {"https://localhost:44301/"}
            };

            //Add new client
            var response = await Client.AddClient(nClient);
            Assert.IsTrue(response);

            //Get new client details
            var response1 = await Client.GetClientDetails(nClient.ClientId);
            var jObj1 = JsonConvert.DeserializeObject(response1);
            var uObj1 = JsonConvert.DeserializeObject<ClientModel>(jObj1.ToString());
            Assert.IsNotNull(uObj1);
            Assert.AreEqual(nClient.ClientId, uObj1.ClientId);
            Console.WriteLine(@"New Client Id: " + uObj1.ClientId);

            //Delete new client
            var response2 = await Client.DeleteClient(nClient);
            Assert.IsTrue(response2);

            //Verify deleted new client
            var response3 = await Client.GetClientList();
            var jObj3 = JsonConvert.DeserializeObject(response3);
            var uObj3 = JsonConvert.DeserializeObject<List<ClientModel>>(jObj3.ToString());
            Assert.IsNotNull(uObj3);
            if (!uObj3.Any()) return;
            var uItem = uObj3.FirstOrDefault(a => a.ClientId == nClient.ClientId);
            Assert.IsNull(uItem);
            Console.WriteLine(@"Deleted Client Id: " + nClient.ClientId);
        }

        [Test]
        public async void Disable_And_Enable_New_Client()
        {
            //New client model
            var nClient = new ClientModel()
            {
                ClientId = "test_client",
                ClientName = "Test Client",
                Flow = "Implicit",
                RedirectUris = new List<string> { "https://localhost:44301/" }
            };

            //Add new client
            var response = await Client.AddClient(nClient);
            Assert.IsTrue(response);

            //Get new client details
            var response1 = await Client.GetClientDetails(nClient.ClientId);
            var jObj1 = JsonConvert.DeserializeObject(response1);
            var uObj1 = JsonConvert.DeserializeObject<ClientModel>(jObj1.ToString());
            Assert.IsNotNull(uObj1);
            Assert.AreEqual(nClient.ClientId, uObj1.ClientId);
            Console.WriteLine(@"New Client Id: " + uObj1.ClientId);

            //Disable new client
            var response4 = await Client.DisableClient(nClient);
            Assert.IsTrue(response4);

            //Verify new client is disabled
            var response5 = await Client.GetClientDetails(nClient.ClientId);
            var jObj5 = JsonConvert.DeserializeObject(response5);
            var uObj5 = JsonConvert.DeserializeObject<ClientModel>(jObj5.ToString());
            Assert.IsNotNull(uObj5);
            Assert.AreEqual(nClient.ClientId, uObj5.ClientId);
            Assert.AreEqual(false, uObj5.IsEnabled);
            Console.WriteLine(@"Disabled Client Id: " + uObj5.ClientId);

            //Enable new Client
            var response6 = await Client.EnableClient(nClient);
            Assert.IsTrue(response6);

            //Verify new Client is enabled
            var response7 = await Client.GetClientDetails(nClient.ClientId);
            var jObj7 = JsonConvert.DeserializeObject(response7);
            var uObj7 = JsonConvert.DeserializeObject<ClientModel>(jObj7.ToString());
            Assert.IsNotNull(uObj7);
            Assert.AreEqual(nClient.ClientId, uObj7.ClientId);
            Assert.AreEqual(true, uObj7.IsEnabled);
            Console.WriteLine(@"Enabled Client Id: " + uObj7.ClientId);

            //Delete new Client
            var response2 = await Client.DeleteClient(nClient);
            Assert.IsTrue(response2);

            //Verify deleted new Client
            var response3 = await Client.GetClientList();
            var jObj3 = JsonConvert.DeserializeObject(response3);
            var uObj3 = JsonConvert.DeserializeObject<List<ClientModel>>(jObj3.ToString());
            Assert.IsNotNull(uObj3);
            if (!uObj3.Any()) return;
            var uItem = uObj3.FirstOrDefault(a => a.ClientId == nClient.ClientId);
            Assert.IsNull(uItem);
            Console.WriteLine(@"Deleted Client Id: " + nClient.ClientId);
        }

        [Test]
        public async void Update_New_Client()
        {
            //New client model
            var nClient = new ClientModel()
            {
                ClientId = "test_client",
                ClientName = "Test Client",
                Flow = "Implicit",
                RedirectUris = new List<string> { "https://localhost:44301/" }
            };

            //Add new client
            var response = await Client.AddClient(nClient);
            Assert.IsTrue(response);

            //Get new client details
            var response1 = await Client.GetClientDetails(nClient.ClientId);
            var jObj1 = JsonConvert.DeserializeObject(response1);
            var uObj1 = JsonConvert.DeserializeObject<ClientModel>(jObj1.ToString());
            Assert.IsNotNull(uObj1);
            Assert.AreEqual(nClient.ClientId, uObj1.ClientId);
            Console.WriteLine(@"New Client Id: " + uObj1.ClientId);

            //Update new client
            var tmpClient = nClient;
            tmpClient.ClientName = "Updated Client";
            tmpClient.Flow = "ClientCredentials";
            var response4 = await Client.UpdateClient(tmpClient);
            Assert.IsTrue(response4);

            //Verify update to new client details
            var response5 = await Client.GetClientDetails(nClient.ClientId);
            var jObj5 = JsonConvert.DeserializeObject(response5);
            var uObj5 = JsonConvert.DeserializeObject<ClientModel>(jObj5.ToString());
            Assert.IsNotNull(uObj5);
            Assert.AreEqual(tmpClient.ClientId, uObj5.ClientId);
            Assert.AreEqual(tmpClient.ClientName, uObj5.ClientName);
            Assert.AreEqual(tmpClient.Flow, uObj5.Flow);
            Console.WriteLine(@"Updated Client Id: " + uObj5.ClientId);

            //Delete new client
            var response2 = await Client.DeleteClient(nClient);
            Assert.IsTrue(response2);

            //Verify deleted new client
            var response3 = await Client.GetClientList();
            var jObj3 = JsonConvert.DeserializeObject(response3);
            var uObj3 = JsonConvert.DeserializeObject<List<ClientModel>>(jObj3.ToString());
            Assert.IsNotNull(uObj3);
            if (!uObj3.Any()) return;
            var uItem = uObj3.FirstOrDefault(a => a.ClientId == nClient.ClientId);
            Assert.IsNull(uItem);
            Console.WriteLine(@"Deleted Client Id: " + nClient.ClientId);
        }
    }
}
