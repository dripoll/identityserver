﻿using System;
using System.Collections.Generic;
using System.Linq;
using IdentityServer.Models;
using Newtonsoft.Json;
using NUnit.Framework;

namespace IdentityServer.Test.Controllers
{
    [TestFixture]
    public class UserController : TestBase
    {
        [Test]
        public async void Login_And_Logout_Local_User()
        {
            //Login user
            var li = await LoginLocalUser();
            Assert.IsTrue(li);

            //Logout user
            var lo = await LogoutLocalUser();
            Assert.IsTrue(lo);

        }

        [Test]
        public async void Login_And_Logout_Client_User()
        {
            //Login client
            var li = await LoginClientUser();
            Assert.IsTrue(li);
        }

        [Test]
        public async void Get_User_Details()
        {
            var response = await Client.GetUserDetails(UserName);
            var jObj = JsonConvert.DeserializeObject(response);
            var uObj = JsonConvert.DeserializeObject<LocalUserModel>(jObj.ToString());
            Assert.IsNotNull(uObj);
            Assert.AreEqual(UserName, uObj.UserName);

            Console.WriteLine(@"Username: " + uObj.UserName);
        }

        [Test]
        public async void Get_User_List()
        {
            var response = await Client.GetUserList();
            var jObj = JsonConvert.DeserializeObject(response);
            var uObj = JsonConvert.DeserializeObject<List<LocalUserModel>>(jObj.ToString());
            Assert.IsNotNull(uObj);

            Console.WriteLine(@"User Count: " + uObj.Count());

            if (!uObj.Any()) return;

            var uItem = uObj.FirstOrDefault(a => a.UserName == UserName);
            Assert.IsNotNull(uItem);

            Console.WriteLine(@"First Username: " + uObj[0].UserName);
        }

        [Test]
        public async void Add_And_Delete_New_User()
        {
            //New user model
            var nUser = new LocalUserModel()
            {
                UserName = "test.newuser@test.com",
                Password = "Test123!",
                Claims = new ClaimModel()
                {
                    Clients = new List<string>() {"maxrte"},
                    Scopes = new List<string>() { "maxrteApi"},
                    FirstName = "Test",
                    MiddleName = "New",
                    LastName = "User",
                    Email = "test.newuser@test.com"
                }
            };

            //Add new user
            var response = await Client.AddUser(nUser);
            Assert.IsTrue(response);

            //Get new user details
            var response1 = await Client.GetUserDetails(nUser.UserName);
            var jObj1 = JsonConvert.DeserializeObject(response1);
            var uObj1 = JsonConvert.DeserializeObject<LocalUserModel>(jObj1.ToString());
            Assert.IsNotNull(uObj1);
            Assert.AreEqual(nUser.UserName, uObj1.UserName);
            Console.WriteLine(@"New User: " + uObj1.UserName);

            //Delete new user
            var response2 = await Client.DeleteUser(nUser);
            Assert.IsTrue(response2);

            //Verify deleted new user
            var response3 = await Client.GetUserList();
            var jObj3 = JsonConvert.DeserializeObject(response3);
            var uObj3 = JsonConvert.DeserializeObject<List<LocalUserModel>>(jObj3.ToString());
            Assert.IsNotNull(uObj3);
            if (!uObj3.Any()) return;
            var uItem = uObj3.FirstOrDefault(a => a.UserName == nUser.UserName);
            Assert.IsNull(uItem);
            Console.WriteLine(@"Deleted User: " + nUser.UserName);

        }

        [Test]
        public async void Disable_And_Enable_New_User()
        {
            //New user model
            var nUser = new LocalUserModel()
            {
                UserName = "test.newuser1@test.com",
                Password = "Test123!",
                Claims = new ClaimModel()
                {
                    Clients = new List<string>() { "maxrte" },
                    Scopes = new List<string>() { "maxrteApi" },
                    FirstName = "Test",
                    MiddleName = "New",
                    LastName = "User1",
                    Email = "test.newuser1@test.com"
                }
            };

            //Add new user
            var response = await Client.AddUser(nUser);
            Assert.IsTrue(response);

            //Get new user details
            var response1 = await Client.GetUserDetails(nUser.UserName);
            var jObj1 = JsonConvert.DeserializeObject(response1);
            var uObj1 = JsonConvert.DeserializeObject<LocalUserModel>(jObj1.ToString());
            Assert.IsNotNull(uObj1);
            Assert.AreEqual(nUser.UserName, uObj1.UserName);
            Assert.AreEqual(true, uObj1.IsEnabled);
            Console.WriteLine(@"New User: " + uObj1.UserName);

            //Disable new user
            var response4 = await Client.DisableUser(nUser);
            Assert.IsTrue(response4);

            //Verify new user is disabled
            var response5 = await Client.GetUserDetails(nUser.UserName);
            var jObj5 = JsonConvert.DeserializeObject(response5);
            var uObj5 = JsonConvert.DeserializeObject<LocalUserModel>(jObj5.ToString());
            Assert.IsNotNull(uObj5);
            Assert.AreEqual(nUser.UserName, uObj5.UserName);
            Assert.AreEqual(false, uObj5.IsEnabled);
            Console.WriteLine(@"Disabled User: " + uObj5.UserName);

            //Enable new user
            var response6 = await Client.EnableUser(nUser);
            Assert.IsTrue(response6);

            //Verify new user is disabled
            var response7 = await Client.GetUserDetails(nUser.UserName);
            var jObj7 = JsonConvert.DeserializeObject(response7);
            var uObj7 = JsonConvert.DeserializeObject<LocalUserModel>(jObj7.ToString());
            Assert.IsNotNull(uObj7);
            Assert.AreEqual(nUser.UserName, uObj7.UserName);
            Assert.AreEqual(true, uObj7.IsEnabled);
            Console.WriteLine(@"Enabled User: " + uObj7.UserName);

            //Delete new user
            var response2 = await Client.DeleteUser(nUser);
            Assert.IsTrue(response2);

            //Verify deleted new user
            var response3 = await Client.GetUserList();
            var jObj3 = JsonConvert.DeserializeObject(response3);
            var uObj3 = JsonConvert.DeserializeObject<List<LocalUserModel>>(jObj3.ToString());
            Assert.IsNotNull(uObj3);
            if (!uObj3.Any()) return;
            var uItem = uObj3.FirstOrDefault(a => a.UserName == nUser.UserName);
            Assert.IsNull(uItem);
            Console.WriteLine(@"Deleted User: " + nUser.UserName);

        }

        [Test]
        public async void Set_Expiration_Date_And_Remove_Expiration_Date()
        {
            //New user model
            var nUser = new LocalUserModel()
            {
                UserName = "test.newuser2@test.com",
                Password = "Test123!",
                Claims = new ClaimModel()
                {
                    Clients = new List<string>() { "maxrte" },
                    Scopes = new List<string>() { "maxrteApi" },
                    FirstName = "Test",
                    MiddleName = "New",
                    LastName = "User2",
                    Email = "test.newuser2@test.com"
                }
            };

            //Add new user
            var response = await Client.AddUser(nUser);
            Assert.IsTrue(response);

            //Get new user details
            var response1 = await Client.GetUserDetails(nUser.UserName);
            var jObj1 = JsonConvert.DeserializeObject(response1);
            var uObj1 = JsonConvert.DeserializeObject<LocalUserModel>(jObj1.ToString());
            Assert.IsNotNull(uObj1);
            Assert.AreEqual(nUser.UserName, uObj1.UserName);
            Assert.AreEqual(true, uObj1.IsEnabled);
            Console.WriteLine(@"New User: " + uObj1.UserName);

            //Set expiration date for new user
            var eDate = DateTime.Now;
            nUser.Claims.Expiration = eDate;
            var response4 = await Client.SetUserExpiration(nUser);
            Assert.IsTrue(response4);

            //Verify expiration date for new user
            var response5 = await Client.GetUserDetails(nUser.UserName);
            var jObj5 = JsonConvert.DeserializeObject(response5);
            var uObj5 = JsonConvert.DeserializeObject<LocalUserModel>(jObj5.ToString());
            Assert.IsNotNull(uObj5);
            Assert.IsNotNull(uObj5.Claims.Expiration);
            Assert.AreEqual(nUser.UserName, uObj5.UserName);
            Assert.AreEqual(eDate.ToString("yyyy-MM-dd HH:mm:ss.fff"), uObj5.Claims.Expiration.Value.ToString("yyyy-MM-dd HH:mm:ss.fff"));
            Console.WriteLine(@"Expired User: " + uObj5.UserName);
            Console.WriteLine(@"Expiration: " + uObj5.Claims.Expiration);

            //Remove expiration date for new user
            nUser.Claims.Expiration = null;
            var response6 = await Client.SetUserExpiration(nUser);
            Assert.IsTrue(response6);

            //Verify expiration date is removed for new user
            var response7 = await Client.GetUserDetails(nUser.UserName);
            var jObj7 = JsonConvert.DeserializeObject(response7);
            var uObj7 = JsonConvert.DeserializeObject<LocalUserModel>(jObj7.ToString());
            Assert.IsNotNull(uObj7);
            Assert.AreEqual(nUser.UserName, uObj7.UserName);
            Assert.IsNull(uObj7.Claims.Expiration);
            Console.WriteLine(@"Unexpired User: " + uObj5.UserName);


            //Delete new user
            var response2 = await Client.DeleteUser(nUser);
            Assert.IsTrue(response2);

            //Verify deleted new user
            var response3 = await Client.GetUserList();
            var jObj3 = JsonConvert.DeserializeObject(response3);
            var uObj3 = JsonConvert.DeserializeObject<List<LocalUserModel>>(jObj3.ToString());
            Assert.IsNotNull(uObj3);
            if (!uObj3.Any()) return;
            var uItem = uObj3.FirstOrDefault(a => a.UserName == nUser.UserName);
            Assert.IsNull(uItem);
            Console.WriteLine(@"Deleted User: " + nUser.UserName);

        }

        [Test]
        public async void Update_New_User()
        {
            //New user model
            var nUser = new LocalUserModel()
            {
                UserName = "test.newuser3@test.com",
                Password = "Test123!",
                Claims = new ClaimModel()
                {
                    Clients = new List<string>() { "maxrte" },
                    Scopes = new List<string>() { "maxrteApi" },
                    FirstName = "Test",
                    MiddleName = "New",
                    LastName = "User3",
                    Email = "test.newuser3@test.com"
                }
            };

            //Add new user
            var response = await Client.AddUser(nUser);
            Assert.IsTrue(response);

            //Get new user details
            var response1 = await Client.GetUserDetails(nUser.UserName);
            var jObj1 = JsonConvert.DeserializeObject(response1);
            var uObj1 = JsonConvert.DeserializeObject<LocalUserModel>(jObj1.ToString());
            Assert.IsNotNull(uObj1);
            Assert.AreEqual(nUser.UserName, uObj1.UserName);
            Assert.AreEqual(true, uObj1.IsEnabled);
            Console.WriteLine(@"New User: " + uObj1.UserName);

            //Update new user details
            nUser.Claims.FirstName = "Changed";
            nUser.Claims.LastName = "Name";
            var response4 = await Client.UpdateUser(nUser);
            Assert.IsTrue(response4);

            //Verify updates for new user
            var response5 = await Client.GetUserDetails(nUser.UserName);
            var jObj5 = JsonConvert.DeserializeObject(response5);
            var uObj5 = JsonConvert.DeserializeObject<LocalUserModel>(jObj5.ToString());
            Assert.IsNotNull(uObj5);
            Assert.AreEqual(nUser.Claims.FirstName,uObj5.Claims.FirstName);
            Assert.AreEqual(nUser.Claims.LastName, uObj5.Claims.LastName);
            Console.WriteLine(@"Updated User: " + uObj5.Claims.FirstName + @" " + uObj5.Claims.LastName);

            //Delete new user
            var response2 = await Client.DeleteUser(nUser);
            Assert.IsTrue(response2);

            //Verify deleted new user
            var response3 = await Client.GetUserList();
            var jObj3 = JsonConvert.DeserializeObject(response3);
            var uObj3 = JsonConvert.DeserializeObject<List<LocalUserModel>>(jObj3.ToString());
            Assert.IsNotNull(uObj3);
            if (!uObj3.Any()) return;
            var uItem = uObj3.FirstOrDefault(a => a.UserName == nUser.UserName);
            Assert.IsNull(uItem);
            Console.WriteLine(@"Deleted User: " + nUser.UserName);

        }

        [Test]
        public async void Add_And_Remove_Client_From_User()
        {
            //New user model
            var nUser = new LocalUserModel()
            {
                UserName = "test.newuser4@test.com",
                Password = "Test123!",
                Claims = new ClaimModel()
                {
                    Clients = new List<string>() { "maxrte" },
                    Scopes = new List<string>() { "maxrteApi" },
                    FirstName = "Test",
                    MiddleName = "New",
                    LastName = "User4",
                    Email = "test.newuser4@test.com"
                }
            };

            //Add new user
            var response = await Client.AddUser(nUser);
            Assert.IsTrue(response);

            //Get new user details
            var response1 = await Client.GetUserDetails(nUser.UserName);
            var jObj1 = JsonConvert.DeserializeObject(response1);
            var uObj1 = JsonConvert.DeserializeObject<LocalUserModel>(jObj1.ToString());
            Assert.IsNotNull(uObj1);
            Assert.AreEqual(nUser.UserName, uObj1.UserName);
            Assert.AreEqual(true, uObj1.IsEnabled);
            Console.WriteLine(@"New User: " + uObj1.UserName);

            //Add client to user
            var tmpUser = nUser;
            tmpUser.Claims.Clients = new List<string> {"test"};
            var response4 = await Client.AddUserClients(tmpUser);
            Assert.IsTrue(response4);

            //Verify added client for new user
            var response5 = await Client.GetUserDetails(nUser.UserName);
            var jObj5 = JsonConvert.DeserializeObject(response5);
            var uObj5 = JsonConvert.DeserializeObject<LocalUserModel>(jObj5.ToString());
            Assert.IsNotNull(uObj5);
            var cItem = uObj5.Claims.Clients.FirstOrDefault(a => a.Contains("test"));
            Assert.IsNotNull(cItem);
            Assert.AreEqual("test", cItem);
            Console.WriteLine(@"Added Client: " + cItem);

            //Remove client from user
            var tmpUser1 = nUser;
            tmpUser1.Claims.Clients = new List<string> { "test" };
            var response6 = await Client.RemoveUserClients(tmpUser1);
            Assert.IsTrue(response6);

            //Verify removed client from new user
            var response7 = await Client.GetUserDetails(nUser.UserName);
            var jObj7 = JsonConvert.DeserializeObject(response7);
            var uObj7 = JsonConvert.DeserializeObject<LocalUserModel>(jObj7.ToString());
            Assert.IsNotNull(uObj7);

            var cItem1 = uObj7.Claims.Clients.FirstOrDefault(a => a.Contains("test"));
            Assert.IsNull(cItem1);

            Console.WriteLine(@"Removed Client: test");

            //Delete new user
            var response2 = await Client.DeleteUser(nUser);
            Assert.IsTrue(response2);

            //Verify deleted new user
            var response3 = await Client.GetUserList();
            var jObj3 = JsonConvert.DeserializeObject(response3);
            var uObj3 = JsonConvert.DeserializeObject<List<LocalUserModel>>(jObj3.ToString());
            Assert.IsNotNull(uObj3);
            if (!uObj3.Any()) return;
            var uItem = uObj3.FirstOrDefault(a => a.UserName == nUser.UserName);
            Assert.IsNull(uItem);
            Console.WriteLine(@"Deleted User: " + nUser.UserName);

        }

        [Test]
        public async void Add_And_Remove_Role_From_User()
        {
            //New user model
            var nUser = new LocalUserModel()
            {
                UserName = "test.newuser5@test.com",
                Password = "Test123!",
                Claims = new ClaimModel()
                {
                    Clients = new List<string>() { "maxrte" },
                    Scopes = new List<string>() { "maxrteApi" },
                    FirstName = "Test",
                    MiddleName = "New",
                    LastName = "User5",
                    Email = "test.newuser5@test.com"
                }
            };

            //Add new user
            var response = await Client.AddUser(nUser);
            Assert.IsTrue(response);

            //Get new user details
            var response1 = await Client.GetUserDetails(nUser.UserName);
            var jObj1 = JsonConvert.DeserializeObject(response1);
            var uObj1 = JsonConvert.DeserializeObject<LocalUserModel>(jObj1.ToString());
            Assert.IsNotNull(uObj1);
            Assert.AreEqual(nUser.UserName, uObj1.UserName);
            Assert.AreEqual(true, uObj1.IsEnabled);
            Console.WriteLine(@"New User: " + uObj1.UserName);

            //Add role to user
            var tmpUser = nUser;
            tmpUser.Claims.Roles = new List<string> { "test role" };
            var response4 = await Client.AddUserRoles(tmpUser);
            Assert.IsTrue(response4);

            //Verify added role for new user
            var response5 = await Client.GetUserDetails(nUser.UserName);
            var jObj5 = JsonConvert.DeserializeObject(response5);
            var uObj5 = JsonConvert.DeserializeObject<LocalUserModel>(jObj5.ToString());
            Assert.IsNotNull(uObj5);
            var cItem = uObj5.Claims.Roles.FirstOrDefault(a => a.Contains("test role"));
            Assert.IsNotNull(cItem);
            Assert.AreEqual("test role", cItem);
            Console.WriteLine(@"Added Role: " + cItem);

            //Remove role from user
            var tmpUser1 = nUser;
            tmpUser1.Claims.Roles = new List<string> { "test role" };
            var response6 = await Client.RemoveUserRoles(tmpUser1);
            Assert.IsTrue(response6);

            //Verify removed role from new user
            var response7 = await Client.GetUserDetails(nUser.UserName);
            var jObj7 = JsonConvert.DeserializeObject(response7);
            var uObj7 = JsonConvert.DeserializeObject<LocalUserModel>(jObj7.ToString());
            Assert.IsNotNull(uObj7);
            var cItem1 = uObj7.Claims.Roles.FirstOrDefault(a => a.Contains("test role"));
            Assert.IsNull(cItem1);
            Console.WriteLine(@"Removed Role: test role");

            //Delete new user
            var response2 = await Client.DeleteUser(nUser);
            Assert.IsTrue(response2);

            //Verify deleted new user
            var response3 = await Client.GetUserList();
            var jObj3 = JsonConvert.DeserializeObject(response3);
            var uObj3 = JsonConvert.DeserializeObject<List<LocalUserModel>>(jObj3.ToString());
            Assert.IsNotNull(uObj3);
            if (!uObj3.Any()) return;
            var uItem = uObj3.FirstOrDefault(a => a.UserName == nUser.UserName);
            Assert.IsNull(uItem);
            Console.WriteLine(@"Deleted User: " + nUser.UserName);

        }

        [Test]
        public async void Add_And_Remove_Scope_From_User()
        {
            //New user model
            var nUser = new LocalUserModel()
            {
                UserName = "test.newuser6@test.com",
                Password = "Test123!",
                Claims = new ClaimModel()
                {
                    Clients = new List<string>() { "maxrte" },
                    Scopes = new List<string>() { "maxrteApi" },
                    FirstName = "Test",
                    MiddleName = "New",
                    LastName = "User6",
                    Email = "test.newuser6@test.com"
                }
            };

            //Add new user
            var response = await Client.AddUser(nUser);
            Assert.IsTrue(response);

            //Get new user details
            var response1 = await Client.GetUserDetails(nUser.UserName);
            var jObj1 = JsonConvert.DeserializeObject(response1);
            var uObj1 = JsonConvert.DeserializeObject<LocalUserModel>(jObj1.ToString());
            Assert.IsNotNull(uObj1);
            Assert.AreEqual(nUser.UserName, uObj1.UserName);
            Assert.AreEqual(true, uObj1.IsEnabled);
            Console.WriteLine(@"New User: " + uObj1.UserName);

            //Add scope to user
            var tmpUser = nUser;
            tmpUser.Claims.Scopes = new List<string> { "test_service" };
            var response4 = await Client.AddUserScopes(tmpUser);
            Assert.IsTrue(response4);

            //Verify added scope for new user
            var response5 = await Client.GetUserDetails(nUser.UserName);
            var jObj5 = JsonConvert.DeserializeObject(response5);
            var uObj5 = JsonConvert.DeserializeObject<LocalUserModel>(jObj5.ToString());
            Assert.IsNotNull(uObj5);
            var cItem = uObj5.Claims.Scopes.FirstOrDefault(a => a.Contains("test_service"));
            Assert.IsNotNull(cItem);
            Assert.AreEqual("test_service", cItem);
            Console.WriteLine(@"Added Scope: " + cItem);

            //Remove scope from user
            var tmpUser1 = nUser;
            tmpUser1.Claims.Scopes = new List<string> { "test_service" };
            var response6 = await Client.RemoveUserScopes(tmpUser1);
            Assert.IsTrue(response6);

            //Verify removed client from new user
            var response7 = await Client.GetUserDetails(nUser.UserName);
            var jObj7 = JsonConvert.DeserializeObject(response7);
            var uObj7 = JsonConvert.DeserializeObject<LocalUserModel>(jObj7.ToString());
            Assert.IsNotNull(uObj7);

            var cItem1 = uObj7.Claims.Scopes.FirstOrDefault(a => a.Contains("test_service"));
            Assert.IsNull(cItem1);

            Console.WriteLine(@"Removed Scope: test_service");

            //Delete new user
            var response2 = await Client.DeleteUser(nUser);
            Assert.IsTrue(response2);

            //Verify deleted new user
            var response3 = await Client.GetUserList();
            var jObj3 = JsonConvert.DeserializeObject(response3);
            var uObj3 = JsonConvert.DeserializeObject<List<LocalUserModel>>(jObj3.ToString());
            Assert.IsNotNull(uObj3);
            if (!uObj3.Any()) return;
            var uItem = uObj3.FirstOrDefault(a => a.UserName == nUser.UserName);
            Assert.IsNull(uItem);
            Console.WriteLine(@"Deleted User: " + nUser.UserName);

        }

        [Test]
        public async void Add_And_Remove_External_User_Ids_From_User()
        {
            //New user model
            var nUser = new LocalUserModel()
            {
                UserName = "test.newuser7@test.com",
                Password = "Test123!",
                Claims = new ClaimModel()
                {
                    Clients = new List<string>() { "maxrte" },
                    Scopes = new List<string>() { "maxrteApi" },
                    FirstName = "Test",
                    MiddleName = "New",
                    LastName = "User7",
                    Email = "test.newuser7@test.com"
                }
            };

            //Add new user
            var response = await Client.AddUser(nUser);
            Assert.IsTrue(response);

            //Get new user details
            var response1 = await Client.GetUserDetails(nUser.UserName);
            var jObj1 = JsonConvert.DeserializeObject(response1);
            var uObj1 = JsonConvert.DeserializeObject<LocalUserModel>(jObj1.ToString());
            Assert.IsNotNull(uObj1);
            Assert.AreEqual(nUser.UserName, uObj1.UserName);
            Assert.AreEqual(true, uObj1.IsEnabled);
            Console.WriteLine(@"New User: " + uObj1.UserName);

            //Add external user id to user
            var tmpUser = nUser;
            tmpUser.Claims.ExternalProviderUserIds = new List<string> { "testuser7" };
            var response4 = await Client.AddUserExternalUserIds(tmpUser);
            Assert.IsTrue(response4);

            //Verify added external user id for new user
            var response5 = await Client.GetUserDetails(nUser.UserName);
            var jObj5 = JsonConvert.DeserializeObject(response5);
            var uObj5 = JsonConvert.DeserializeObject<LocalUserModel>(jObj5.ToString());
            Assert.IsNotNull(uObj5);
            var cItem = uObj5.Claims.ExternalProviderUserIds.FirstOrDefault(a => a.Contains("testuser7"));
            Assert.IsNotNull(cItem);
            Assert.AreEqual("testuser7", cItem);
            Console.WriteLine(@"Added External User Id: " + cItem);

            //Remove external user id from user
            var tmpUser1 = nUser;
            tmpUser1.Claims.ExternalProviderUserIds = new List<string> { "testuser7" };
            var response6 = await Client.RemoveUserExternalUserIds(tmpUser1);
            Assert.IsTrue(response6);

            //Verify removed external user id from new user
            var response7 = await Client.GetUserDetails(nUser.UserName);
            var jObj7 = JsonConvert.DeserializeObject(response7);
            var uObj7 = JsonConvert.DeserializeObject<LocalUserModel>(jObj7.ToString());
            Assert.IsNotNull(uObj7);

            var cItem1 = uObj7.Claims.ExternalProviderUserIds.FirstOrDefault(a => a.Contains("testuser7"));
            Assert.IsNull(cItem1);

            Console.WriteLine(@"Removed External User Id: testuser7");

            //Delete new user
            var response2 = await Client.DeleteUser(nUser);
            Assert.IsTrue(response2);

            //Verify deleted new user
            var response3 = await Client.GetUserList();
            var jObj3 = JsonConvert.DeserializeObject(response3);
            var uObj3 = JsonConvert.DeserializeObject<List<LocalUserModel>>(jObj3.ToString());
            Assert.IsNotNull(uObj3);
            if (!uObj3.Any()) return;
            var uItem = uObj3.FirstOrDefault(a => a.UserName == nUser.UserName);
            Assert.IsNull(uItem);
            Console.WriteLine(@"Deleted User: " + nUser.UserName);

        }

        [Test]
        public async void Change_User_Password()
        {
            //New user model
            var nUser = new LocalUserModel()
            {
                UserName = "test.newuser8@test.com",
                Password = "Test123!",
                Claims = new ClaimModel()
                {
                    Clients = new List<string>() { "maxrte" },
                    Scopes = new List<string>() { "maxrteApi" },
                    FirstName = "Test",
                    MiddleName = "New",
                    LastName = "User8",
                    Email = "test.newuser8@test.com"
                }
            };

            //Add new user
            var response = await Client.AddUser(nUser);
            Assert.IsTrue(response);

            //Get new user details
            var response1 = await Client.GetUserDetails(nUser.UserName);
            var jObj1 = JsonConvert.DeserializeObject(response1);
            var uObj1 = JsonConvert.DeserializeObject<LocalUserModel>(jObj1.ToString());
            Assert.IsNotNull(uObj1);
            Assert.AreEqual(nUser.UserName, uObj1.UserName);
            Console.WriteLine(@"New User: " + uObj1.UserName);

            //Login user
            var ulModel = new UserLoginModel()
            {               
                UserName = nUser.UserName,
                Password = nUser.Password,
                ClientId = nUser.Claims.Clients[0]
            };
            var li = await Client.LoginLocalUser(ulModel);
            Assert.IsTrue(li);

            //Logout user
            var lo = await LogoutLocalUser();
            Assert.IsTrue(lo);

            //Change user password
            var nPwd = new UserChangePasswordModel()
            {
                UserName = nUser.UserName,
                OldPassword = nUser.Password,
                NewPassword = "123Test!",
                ConfirmNewPassword = "123Test!"
            };
            var response4 = await Client.ChangeUserPassword(nPwd);
            Assert.IsTrue(response4);

            //Login user with new password
            var ulModel1 = new UserLoginModel()
            {
                UserName = nUser.UserName,
                Password = "123Test!",
                ClientId = nUser.Claims.Clients[0]
            };
            var li1 = await Client.LoginLocalUser(ulModel1);
            Assert.IsTrue(li1);

            //Logout user
            var lo1 = await LogoutLocalUser();
            Assert.IsTrue(lo1);

            //Delete new user
            var response2 = await Client.DeleteUser(nUser);
            Assert.IsTrue(response2);

            //Verify deleted new user
            var response3 = await Client.GetUserList();
            var jObj3 = JsonConvert.DeserializeObject(response3);
            var uObj3 = JsonConvert.DeserializeObject<List<LocalUserModel>>(jObj3.ToString());
            Assert.IsNotNull(uObj3);
            if (!uObj3.Any()) return;
            var uItem = uObj3.FirstOrDefault(a => a.UserName == nUser.UserName);
            Assert.IsNull(uItem);
            Console.WriteLine(@"Deleted User: " + nUser.UserName);

        }

    }
}
